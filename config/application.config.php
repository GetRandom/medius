<?php

return array(
    'modules' => array(
        'ZendDeveloperTools',
        'DoctrineModule',
        'DoctrineORMModule',
        'BjyAuthorize',
        'Application',
        'Staff',
        'Common',
        'Medcard',
        'ICD',
    ),
    'module_listener_options' => array(
        'module_paths' => array(
            './module',
            './vendor'
        ),
        'config_glob_paths' => array(
            'config/autoload/{,*.}{global,local}.php'
        )
    ),
    'strategies' => array(
        'ViewJsonStrategy'
    )
);
