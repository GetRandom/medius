<?php

return array(
    'router' => array(
        'routes' => array(
            'api_medcard' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/:controller/:action[/:id]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Medcard\Service\MedcardBuildingService' => 'Medcard\Factory\MedcardBuildingServiceFactory',
        ),
    ),
    'controllers' => array(
        'factories' => array(
            'medcard-download' => 'Medcard\Factory\MedcardDownloadControllerFactory',
            'patient' => 'Medcard\Factory\PatientControllerFactory',
            'treatment' => 'Medcard\Factory\TreatmentControllerFactory',
            'hospitalization' => 'Medcard\Factory\HospitalizationControllerFactory',
            'category' => 'Medcard\Factory\CategoryControllerFactory',
            'operation' => 'Medcard\Factory\OperationControllerFactory',
            'notebook' => 'Medcard\Factory\NotebookControllerFactory',
            'efficiency-mark' => 'Medcard\Factory\EfficiencyMarkControllerFactory',
            'examination-result' => 'Medcard\Factory\ExaminationResultControllerFactory',
            'treatment-diagnosis' => 'Medcard\Factory\TreatmentDiagnosisControllerFactory',
            'location' => 'Medcard\Factory\LocationControllerFactory',
            'region' => 'Medcard\Factory\RegionControllerFactory',
            'locality' => 'Medcard\Factory\LocalityControllerFactory',
            'admission-department' => 'Medcard\Factory\AdmissionDepartmentControllerFactory',
            'contract' => 'Medcard\Factory\ContractControllerFactory',
            'food' => 'Medcard\Factory\FoodControllerFactory',
            'pulse' => 'Medcard\Factory\PulseControllerFactory',
            'temperature' => 'Medcard\Factory\TemperatureControllerFactory',
            'pressure' => 'Medcard\Factory\PressureControllerFactory',
            'expert-inspection' => 'Medcard\Factory\ExpertInspectionControllerFactory',
        ),
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            'medcard_entity' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'paths' => array(__DIR__ . '/../src/Medcard/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Medcard\Entity' => 'medcard_entity',
                ),
            ),
        ),
        'connection' => array(
            'orm_default' => array(
                'doctrine_type_mappings' => array(
                    'enum' => 'string'
                ),
            ),
        ),
    ),
    'bjyauthorize' => array(
        'guards' => array(
            'BjyAuthorize\Guard\Controller' => array(
                /* MEDCARD DOWNLOAD */
                array(
                    'controller' => 'medcard-download',
                    'action' => array('download'),
                    'roles' => array('nurse', 'doctor', 'administrator')
                ),
                /* PATIENT */
                array(
                    'controller' => 'patient',
                    'action' => array('all', 'view'),
                    'roles' => array('nurse')
                ),
                array(
                    'controller' => 'patient',
                    'roles' => array('doctor', 'administrator')
                ),
                /* TREATMENT */
                array(
                    'controller' => 'treatment',
                    'action' => array('all', 'view'),
                    'roles' => array('nurse')
                ),
                array(
                    'controller' => 'treatment',
                    'roles' => array('doctor', 'administrator')
                ),
                /* HOSPITALIZATION */
                array(
                    'controller' => 'hospitalization',
                    'action' => array('all', 'view'),
                    'roles' => array('nurse')
                ),
                array(
                    'controller' => 'hospitalization',
                    'roles' => array('doctor', 'administrator')
                ),
                /* OPERATION */
                array(
                    'controller' => 'operation',
                    'action' => array('all', 'view'),
                    'roles' => array('nurse')
                ),
                array(
                    'controller' => 'operation',
                    'roles' => array('doctor', 'administrator')
                ),
                /* NOTEBOOK */
                array(
                    'controller' => 'notebook',
                    'action' => array('all', 'view'),
                    'roles' => array('nurse')
                ),
                array(
                    'controller' => 'notebook',
                    'roles' => array('doctor', 'administrator')
                ),
                /* EFFICIENCY MARK */
                array(
                    'controller' => 'efficiency-mark',
                    'action' => array('all', 'view'),
                    'roles' => array('nurse')
                ),
                array(
                    'controller' => 'efficiency-mark',
                    'roles' => array('doctor', 'administrator')
                ),
                /* EXAMINATION RESULT */
                array(
                    'controller' => 'examination-result',
                    'action' => array('all', 'view'),
                    'roles' => array('nurse')
                ),
                array(
                    'controller' => 'examination-result',
                    'roles' => array('doctor', 'administrator')
                ),
                /* CATEGORY */
                array(
                    'controller' => 'category',
                    'action' => array('all', 'view'),
                    'roles' => array('nurse', 'doctor')
                ),
                array(
                    'controller' => 'category',
                    'roles' => array('administrator')
                ),
                /* TREATMENT DIAGNOSIS */
                array(
                    'controller' => 'treatment-diagnosis',
                    'action' => array('all', 'view'),
                    'roles' => array('nurse')
                ),
                array(
                    'controller' => 'treatment-diagnosis',
                    'roles' => array('doctor', 'administrator')
                ),
                /* LOCATION */
                array(
                    'controller' => 'location',
                    'action' => array('all', 'view'),
                    'roles' => array('nurse')
                ),
                array(
                    'controller' => 'location',
                    'roles' => array('doctor', 'administrator')
                ),
                /* LOCALITY */
                array(
                    'controller' => 'locality',
                    'action' => array('all', 'view'),
                    'roles' => array()
                ),
                /* REGION */
                array(
                    'controller' => 'region',
                    'action' => array('all'),
                    'roles' => array()
                ),
                /* ADMISSION DEPARTMENT */
                array(
                    'controller' => 'admission-department',
                    'action' => array('all', 'view'),
                    'roles' => array('nurse')
                ),
                array(
                    'controller' => 'admission-department',
                    'roles' => array('doctor', 'administrator')
                ),
                /* CONTRACT */
                array(
                    'controller' => 'contract',
                    'action' => array('all', 'view'),
                    'roles' => array('nurse')
                ),
                array(
                    'controller' => 'contract',
                    'roles' => array('doctor', 'administrator')
                ),
                /* CATEGORY */
                array(
                    'controller' => 'food',
                    'action' => array('all', 'view'),
                    'roles' => array('nurse', 'doctor')
                ),
                array(
                    'controller' => 'food',
                    'roles' => array('administrator')
                ),
                /* PULSE */
                array(
                    'controller' => 'pulse',
                    'roles' => array('nurse', 'doctor', 'administrator')
                ),
                /* TEMPERATURE */
                array(
                    'controller' => 'temperature',
                    'roles' => array('nurse', 'doctor', 'administrator')
                ),
                /* PRESSURE */
                array(
                    'controller' => 'pressure',
                    'roles' => array('nurse', 'doctor', 'administrator')
                ),
                /* NOTEBOOK */
                array(
                    'controller' => 'expert-inspection',
                    'action' => array('all', 'view'),
                    'roles' => array('nurse')
                ),
                array(
                    'controller' => 'expert-inspection',
                    'roles' => array('doctor', 'administrator')
                ),
            ),
        ),
    ),
);
