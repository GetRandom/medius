<?php
namespace Medcard;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function onBootstrap() {
        \Doctrine\DBAL\Types\Type::addType('enumblood', 'Medcard\DBAL\EnumBloodType');
        \Doctrine\DBAL\Types\Type::addType('enumrhfactor', 'Medcard\DBAL\EnumRhFactorType');
        \Doctrine\DBAL\Types\Type::addType('enumfood', 'Medcard\DBAL\EnumFoodType');
        \Doctrine\DBAL\Types\Type::addType('enumtreatment', 'Medcard\DBAL\EnumTreatmentType');
    }
}
