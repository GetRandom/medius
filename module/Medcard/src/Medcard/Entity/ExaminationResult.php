<?php

namespace Medcard\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExaminationResult
 *
 * @ORM\Table(name="examination_result", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_examination_result_treatment1_idx", columns={"treatment_id"})})
 * @ORM\Entity
 */
class ExaminationResult
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="text", nullable=true)
     */
    private $note;

    /**
     * @var \Medcard\Entity\Treatment
     *
     * @ORM\ManyToOne(targetEntity="Medcard\Entity\Treatment", inversedBy="examinationResult")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="treatment_id", referencedColumnName="id")
     * })
     */
    private $treatment;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return ExaminationResult
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return ExaminationResult
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set treatment
     *
     * @param \Medcard\Entity\Treatment $treatment
     * @return ExaminationResult
     */
    public function setTreatment(\Medcard\Entity\Treatment $treatment = null)
    {
        $this->treatment = $treatment;

        return $this;
    }

    /**
     * Get treatment
     *
     * @return \Medcard\Entity\Treatment 
     */
    public function getTreatment()
    {
        return $this->treatment;
    }
}
