<?php

namespace Medcard\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hospitalization
 *
 * @ORM\Table(name="hospitalization", uniqueConstraints={@ORM\UniqueConstraint(name="medcard_id_UNIQUE", columns={"medcard_id"})})
 * @ORM\Entity
 */
class Hospitalization
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="after_hours", type="integer", nullable=true)
     */
    private $afterHours;

    /**
     * @var string
     *
     * @ORM\Column(name="hosp_term", type="string", nullable=true)
     */
    private $hospTerm;

    /**
     * @var string
     *
     * @ORM\Column(name="institution_diagnosis", type="string", length=100, nullable=true)
     */
    private $institutionDiagnosis;

    /**
     * @var string
     *
     * @ORM\Column(name="hospitalization_diagnosis", type="string", length=100, nullable=true)
     */
    private $hospitalizationDiagnosis;

    /**
     * @var string
     *
     * @ORM\Column(name="clinical_diagnosis", type="string", length=100, nullable=true)
     */
    private $clinicalDiagnosis;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="setting_date", type="date", nullable=true)
     */
    private $settingDate;

    /**
     * @var \Common\Entity\Medcard
     *
     * @ORM\OneToOne(targetEntity="Common\Entity\Medcard", inversedBy="hospitalization")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="medcard_id", referencedColumnName="id")
     * })
     */
    private $medcard;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set afterHours
     *
     * @param integer $afterHours
     * @return Hospitalization
     */
    public function setAfterHours($afterHours)
    {
        $this->afterHours = $afterHours;

        return $this;
    }

    /**
     * Get afterHours
     *
     * @return integer 
     */
    public function getAfterHours()
    {
        return $this->afterHours;
    }

    /**
     * Set hospTerm
     *
     * @param string $hospTerm
     * @return Hospitalization
     */
    public function setHospTerm($hospTerm)
    {
        $this->hospTerm = $hospTerm;

        return $this;
    }

    /**
     * Get hospTerm
     *
     * @return string 
     */
    public function getHospTerm()
    {
        return $this->hospTerm;
    }

    /**
     * Set institutionDiagnosis
     *
     * @param string $institutionDiagnosis
     * @return Hospitalization
     */
    public function setInstitutionDiagnosis($institutionDiagnosis)
    {
        $this->institutionDiagnosis = $institutionDiagnosis;

        return $this;
    }

    /**
     * Get institutionDiagnosis
     *
     * @return string 
     */
    public function getInstitutionDiagnosis()
    {
        return $this->institutionDiagnosis;
    }

    /**
     * Set hospitalizationDiagnosis
     *
     * @param string $hospitalizationDiagnosis
     * @return Hospitalization
     */
    public function setHospitalizationDiagnosis($hospitalizationDiagnosis)
    {
        $this->hospitalizationDiagnosis = $hospitalizationDiagnosis;

        return $this;
    }

    /**
     * Get hospitalizationDiagnosis
     *
     * @return string 
     */
    public function getHospitalizationDiagnosis()
    {
        return $this->hospitalizationDiagnosis;
    }

    /**
     * Set clinicalDiagnosis
     *
     * @param string $clinicalDiagnosis
     * @return Hospitalization
     */
    public function setClinicalDiagnosis($clinicalDiagnosis)
    {
        $this->clinicalDiagnosis = $clinicalDiagnosis;

        return $this;
    }

    /**
     * Get clinicalDiagnosis
     *
     * @return string 
     */
    public function getClinicalDiagnosis()
    {
        return $this->clinicalDiagnosis;
    }

    /**
     * Set settingDate
     *
     * @param \DateTime $settingDate
     * @return Hospitalization
     */
    public function setSettingDate($settingDate)
    {
        $this->settingDate = $settingDate;

        return $this;
    }

    /**
     * Get settingDate
     *
     * @return \DateTime 
     */
    public function getSettingDate()
    {
        return $this->settingDate;
    }

    /**
     * Set medcard
     *
     * @param \Common\Entity\Medcard $medcard
     * @return Hospitalization
     */
    public function setMedcard(\Common\Entity\Medcard $medcard = null)
    {
        $this->medcard = $medcard;

        return $this;
    }

    /**
     * Get medcard
     *
     * @return \Common\Entity\Medcard 
     */
    public function getMedcard()
    {
        return $this->medcard;
    }
}
