<?php

namespace Medcard\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Location
 *
 * @ORM\Table(name="location", uniqueConstraints={@ORM\UniqueConstraint(name="patient_id_UNIQUE", columns={"patient_id"})}, indexes={@ORM\Index(name="fk_location_locality1_idx", columns={"locality_id"})})
 * @ORM\Entity
 */
class Location
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="locality_type", type="string", nullable=true)
     */
    private $localityType;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=100, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="area", type="string", length=100, nullable=true)
     */
    private $area;

    /**
     * @var \Medcard\Entity\Locality
     *
     * @ORM\ManyToOne(targetEntity="Medcard\Entity\Locality")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="locality_id", referencedColumnName="id")
     * })
     */
    private $locality;

    /**
     * @var \Medcard\Entity\Patient
     *
     * @ORM\OneToOne(targetEntity="Medcard\Entity\Patient", inversedBy="location")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="patient_id", referencedColumnName="id")
     * })
     */
    private $patient;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set localityType
     *
     * @param string $localityType
     * @return Location
     */
    public function setLocalityType($localityType)
    {
        $this->localityType = $localityType;

        return $this;
    }

    /**
     * Get localityType
     *
     * @return string 
     */
    public function getLocalityType()
    {
        return $this->localityType;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Location
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set area
     *
     * @param string $area
     * @return Location
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return string 
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set locality
     *
     * @param \Medcard\Entity\Locality $locality
     * @return Location
     */
    public function setLocality(\Medcard\Entity\Locality $locality = null)
    {
        $this->locality = $locality;

        return $this;
    }

    /**
     * Get locality
     *
     * @return \Medcard\Entity\Locality 
     */
    public function getLocality()
    {
        return $this->locality;
    }

    /**
     * Set patient
     *
     * @param \Medcard\Entity\Patient $patient
     * @return Location
     */
    public function setPatient(\Medcard\Entity\Patient $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient
     *
     * @return \Medcard\Entity\Patient 
     */
    public function getPatient()
    {
        return $this->patient;
    }
}
