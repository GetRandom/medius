<?php

namespace Medcard\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pressure
 *
 * @ORM\Table(name="pressure", indexes={@ORM\Index(name="fk_pressure_medcard1_idx", columns={"medcard_id"})})
 * @ORM\Entity
 */
class Pressure
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="top", type="integer", nullable=true)
     */
    private $top;

    /**
     * @var integer
     *
     * @ORM\Column(name="lower", type="integer", nullable=true)
     */
    private $lower;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;
    
    /**
     * @var \Common\Entity\Medcard
     *
     * @ORM\ManyToOne(targetEntity="Common\Entity\Medcard")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="medcard_id", referencedColumnName="id")
     * })
     */
    private $medcard;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set top
     *
     * @param integer $top
     * @return Pressure
     */
    public function setTop($top)
    {
        $this->top = $top;

        return $this;
    }

    /**
     * Get top
     *
     * @return integer 
     */
    public function getTop()
    {
        return $this->top;
    }

    /**
     * Set lower
     *
     * @param integer $lower
     * @return Pressure
     */
    public function setLower($lower)
    {
        $this->lower = $lower;

        return $this;
    }

    /**
     * Get lower
     *
     * @return integer 
     */
    public function getLower()
    {
        return $this->lower;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Pressure
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }
    
    /**
     * Set medcard
     *
     * @param \Common\Entity\Medcard $medcard
     * @return Pressure
     */
    public function setMedcard(\Common\Entity\Medcard $medcard = null)
    {
        $this->medcard = $medcard;

        return $this;
    }

    /**
     * Get medcard
     *
     * @return \Common\Entity\Medcard 
     */
    public function getMedcard()
    {
        return $this->medcard;
    }
}
