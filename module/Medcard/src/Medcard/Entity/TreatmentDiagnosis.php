<?php

namespace Medcard\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TreatmentDiagnosis
 *
 * @ORM\Table(name="treatment_diagnosis", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"}), @ORM\UniqueConstraint(name="treatment_id_UNIQUE", columns={"treatment_id"})})
 * @ORM\Entity
 */
class TreatmentDiagnosis
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="final", type="text", nullable=true)
     */
    private $final;

    /**
     * @var string
     *
     * @ORM\Column(name="main", type="text", nullable=true)
     */
    private $main;

    /**
     * @var string
     *
     * @ORM\Column(name="complication", type="text", nullable=true)
     */
    private $complication;

    /**
     * @var string
     *
     * @ORM\Column(name="related", type="text", nullable=true)
     */
    private $related;

    /**
     * @var \Medcard\Entity\Treatment
     *
     * @ORM\OneToOne(targetEntity="Medcard\Entity\Treatment", inversedBy="treatmentDiagnosis")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="treatment_id", referencedColumnName="id")
     * })
     */
    private $treatment;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set final
     *
     * @param string $final
     * @return TreatmentDiagnosis
     */
    public function setFinal($final)
    {
        $this->final = $final;

        return $this;
    }

    /**
     * Get final
     *
     * @return string 
     */
    public function getFinal()
    {
        return $this->final;
    }

    /**
     * Set main
     *
     * @param string $main
     * @return TreatmentDiagnosis
     */
    public function setMain($main)
    {
        $this->main = $main;

        return $this;
    }

    /**
     * Get main
     *
     * @return string 
     */
    public function getMain()
    {
        return $this->main;
    }

    /**
     * Set complication
     *
     * @param string $complication
     * @return TreatmentDiagnosis
     */
    public function setComplication($complication)
    {
        $this->complication = $complication;

        return $this;
    }

    /**
     * Get complication
     *
     * @return string 
     */
    public function getComplication()
    {
        return $this->complication;
    }

    /**
     * Set related
     *
     * @param string $related
     * @return TreatmentDiagnosis
     */
    public function setRelated($related)
    {
        $this->related = $related;

        return $this;
    }

    /**
     * Get related
     *
     * @return string 
     */
    public function getRelated()
    {
        return $this->related;
    }

    /**
     * Set treatment
     *
     * @param \Medcard\Entity\Treatment $treatment
     * @return TreatmentDiagnosis
     */
    public function setTreatment(\Medcard\Entity\Treatment $treatment = null)
    {
        $this->treatment = $treatment;

        return $this;
    }

    /**
     * Get treatment
     *
     * @return \Medcard\Entity\Treatment 
     */
    public function getTreatment()
    {
        return $this->treatment;
    }
}
