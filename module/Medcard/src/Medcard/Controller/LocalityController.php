<?php

namespace Medcard\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class LocalityController extends AbstractActionController {

// PUBLIC:
    const ENTITY_NAME = '\Medcard\Entity\Locality';

    public function allAction() {
        $id = $this->params('id');
        $data = $this->entityManager->createQuery('SELECT e FROM ' . self::ENTITY_NAME . ' e WHERE e.region = ?1')
                        ->setParameter(1, $id)->getArrayResult();
        return new JsonModel($data);
    }

    public function viewAction() {
        $data = [];
        $id = $this->params('id');
        $object = $this->entityManager->getRepository(self::ENTITY_NAME)->find($id);
        if ($object) {
            $data = $this->hydrator->extract($object);
            if ($object->getRegion()) {
                $data['region'] = $object->getRegion()->getId();
            } else {
                $data['region'] = null;
            }
        }
        return new JsonModel($data);
    }

    public function setEntityManager($entityManager) {
        $this->entityManager = $entityManager;
        $this->hydrator = new DoctrineHydrator($this->entityManager, self::ENTITY_NAME);
    }

// PROTECTED:
    protected $entityManager;
    protected $hydrator;

}
