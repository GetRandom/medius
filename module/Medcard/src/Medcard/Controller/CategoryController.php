<?php

namespace Medcard\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class CategoryController extends AbstractActionController {

    protected $entityManager;
    protected $hydrator;


    public function allAction() {
        $data = $this->entityManager->createQuery('SELECT c FROM \Medcard\Entity\Category c')->getArrayResult();
        return new JsonModel($data);
    }
    
    public function viewAction() {
        $id = $this->params('id');
        $data = $this->entityManager->createQuery('SELECT c FROM \Medcard\Entity\Category c WHERE c.id = ?1')
                ->setParameter(1, $id)->getSingleResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
        return new JsonModel($data);
    }
    
    public function addAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $object = $this->hydrator->hydrate($data, new \Medcard\Entity\Category());
            $this->entityManager->persist($object);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }
    
    public function updateAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $object = $this->hydrator->hydrate($data, new \Medcard\Entity\Category());
            $this->entityManager->merge($object);
            $this->entityManager->persist($object);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }
    
    public function removeAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $object = $this->hydrator->hydrate($data, new \Medcard\Entity\Category());
            $this->entityManager->merge($object);
            $this->entityManager->remove($object);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }
    
    public function setEntityManager($entityManager) {
        $this->entityManager = $entityManager;
        $this->hydrator = new DoctrineHydrator($this->entityManager, '\Medcard\Entity\Category');
    }

}
