<?php

namespace Medcard\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class HospitalizationController extends AbstractActionController {

// PUBLIC:
    const ENTITY_NAME = '\Medcard\Entity\Hospitalization';

    public function viewAction() {
        $id = $this->params()->fromRoute('id');
        $object = $this->entityManager->getRepository(self::ENTITY_NAME)->findOneBy(array('medcard' => $id));
        $data = $this->hydrator->extract($object);
        $data['medcard'] = $object->getMedcard()->getId();
        return new JsonModel($data);
    }

    public function updateAction() {
        if ($this->getRequest()->isPost()) {
            $data = $this->getJsonContent();
            $object = $this->hydrator->hydrate($data, new \Medcard\Entity\Hospitalization());
            $this->entityManager->merge($object);
            $this->entityManager->persist($object);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }

    public function setEntityManager($entityManager) {
        $this->entityManager = $entityManager;
        $this->hydrator = new DoctrineHydrator($this->entityManager, self::ENTITY_NAME, false);
    }

// PROTECTED:
    protected $entityManager;
    protected $hydrator;
    
    protected function getJsonContent() {
        return json_decode($this->getRequest()->getContent(), true);
    }

}
