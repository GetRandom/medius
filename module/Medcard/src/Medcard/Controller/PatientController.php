<?php

namespace Medcard\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class PatientController extends AbstractActionController {

// PUBLIC:
    const ENTITY_NAME = '\Medcard\Entity\Patient';

    public function allAction() {
        $objects = $this->entityManager->getRepository(self::ENTITY_NAME)->findAll();
        foreach ($objects as $key => $object) {
            $data[$key] = $this->extract($object);
        }
        return new JsonModel($data);
    }

    public function viewAction() {
        $id = $this->params()->fromRoute('id');
        $object = $this->entityManager->getRepository(self::ENTITY_NAME)->findOneBy(array('medcard' => $id));
        $data = $this->extract($object);
        return new JsonModel($data);
    }

    public function updateAction() {
        if ($this->getRequest()->isPost()) {
            $data = $this->getJsonContent();
            if ($data['category'] === null) {
                unset($data['category']);
            }
            $object = $this->hydrator->hydrate($data, new \Medcard\Entity\Patient);
            $this->entityManager->merge($object);
            $this->entityManager->persist($object);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }

    public function setEntityManager($entityManager) {
        $this->entityManager = $entityManager;
        $this->hydrator = new DoctrineHydrator($this->entityManager, self::ENTITY_NAME);
    }

// PROTECTED:
    protected $entityManager;
    protected $hydrator;

    protected function getJsonContent() {
        return json_decode($this->getRequest()->getContent(), true);
    }

// PRIVATE:
    private function extract($object) {
        $data = $this->hydrator->extract($object);
        $data['medcard'] = $object->getMedcard()->getId();
        $data['category'] = null;
        if ($object->getCategory()) {
            $data['category'] = $object->getCategory()->getId();
        }
        return $data;
    }

}
