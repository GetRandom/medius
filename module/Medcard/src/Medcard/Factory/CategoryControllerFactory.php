<?php

namespace Medcard\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Medcard\Controller\CategoryController;

class CategoryControllerFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {
        $controller = new CategoryController();
        $controller->setEntityManager(
                $serviceLocator->getServiceLocator()->get('Doctrine\ORM\EntityManager')
        );
        return $controller;
    }

}
