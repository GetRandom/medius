<?php

namespace Medcard\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Medcard\Controller\PressureController;

class PressureControllerFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {
        $controller = new PressureController();
        $controller->setEntityManager(
                $serviceLocator->getServiceLocator()->get('Doctrine\ORM\EntityManager')
        );
        return $controller;
    }

}
