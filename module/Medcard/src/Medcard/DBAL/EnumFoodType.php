<?php
namespace Medcard\DBAL;

class EnumFoodType extends \Medcard\DBAL\EnumType
{
    protected $name = 'enumfood';
    protected $values = array('with_food', 'without_food');
}