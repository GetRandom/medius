<?php
namespace Medcard\DBAL;

class EnumBloodType extends EnumType
{
    protected $name = 'enumblood';
    protected $values = array('1', '2', '3', '4');
}