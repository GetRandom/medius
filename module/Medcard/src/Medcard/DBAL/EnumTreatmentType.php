<?php
namespace Medcard\DBAL;

class EnumTreatmentType extends \Medcard\DBAL\EnumType
{
    protected $name = 'enumtreatment';
    protected $values = array('operational', 'not_operational', 'not_defined');
}