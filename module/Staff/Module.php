<?php

namespace Staff;

use BjyAuthorize\View\RedirectionStrategy;
use Zend\EventManager\EventInterface;

class Module {

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function onBootstrap(EventInterface $e) {
        $application = $e->getTarget();
        $eventManager = $application->getEventManager();
        $strategy = new RedirectionStrategy();
        $strategy->setRedirectRoute('staff_login');
        $eventManager->attach($strategy);

        // 404 error
        $eventManager->attach(\Zend\Mvc\MvcEvent::EVENT_DISPATCH_ERROR, function($e) {
            $result = $e->getResult();
            $result->setTemplate('error/404');
            $result->setTerminal(TRUE);
        });
    }

}
