<?php

namespace Staff\Provider\Identity;

//use Zend\Permissions\Acl\Role\RoleInterface;
use BjyAuthorize\Provider\Identity\ProviderInterface;

class StaffIdentityProvider implements ProviderInterface {

    protected $entityManager;
    protected $authService;

    protected $defaultRole = 'guest';

    public function __construct($entityManager, $authService) {
        $this->entityManager = $entityManager;
        $this->authService = $authService;
    }

    public function getIdentityRoles() {

        if (!$this->authService->hasIdentity()) {
            return array($this->getDefaultRole());
        } else {
            return $this->authService->getIdentity()->getRoles();
        }
    }

    public function getDefaultRole() {
        return $this->defaultRole;
    }

    public function setDefaultRole($defaultRole) {
        $this->defaultRole = $defaultRole;
    }

}
