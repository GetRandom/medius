<?php

namespace Staff\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Staff\Provider\Identity\StaffIdentityProvider;

class StaffIdentityProviderFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {
        $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');
        $authService = $serviceLocator->get('Staff\Service\AuthService');
        $provider = new StaffIdentityProvider($entityManager, $authService);
        return $provider;
    }

}
