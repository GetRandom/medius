<?php

namespace Staff\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class AuthController extends AbstractActionController {

    protected $entityManager;
    protected $authService;

    public function __construct($entityManager, $authService) {
        $this->entityManager = $entityManager;
        $this->authService = $authService;
    }

    public function loginAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $this->authService->setCredential($data['login'], $data['password']);
            if ($this->authService->authenticate()) {
                $identity = $this->authService->getIdentity();
                $roles = $identity->getRoles();
                $roleTitles = array();
                $header = new \Zend\Http\Header\SetCookie();
                $header->setName('role');
                $header->setPath('/');
                $header->setExpires(time() + 999999);
                $this->getResponse()->getHeaders()->addHeader($header);
                foreach ($roles as $role) {
                    $title = $role->getRoleTitle();
                    $roleTitles[] = $title;
                    $header->setValue($title);
                }
                return new JsonModel(array('data' => $roleTitles));
            }
        }
        $this->getResponse()->setStatusCode(400);
        return new JsonModel();
    }

    public function logoutAction() {
        $header = new \Zend\Http\Header\SetCookie();
        $header->setName('role');
        $header->setPath('/');
        $header->setExpires(time() - 999999);
        $this->getResponse()->getHeaders()->addHeader($header);
        $this->authService->clearIdentity();
        return new JsonModel();
    }

}
