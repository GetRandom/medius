<?php

namespace Staff\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class DigestController extends AbstractActionController {

    private $entityManager;

    public function getPostsAction() {
        $data = $this->entityManager->getRepository('\Staff\Entity\Post')->findAllAsArray();
        return new JsonModel(array('data' => $data));
    }

    public function getSeparationsAction() {
        $data = $this->entityManager->getRepository('\Staff\Entity\Separation')->findAllAsArray();
        return new JsonModel(array('data' => $data));
    }

    public function getWorkplacesAction() {
        $data = $this->entityManager->getRepository('\Staff\Entity\Workplace')->findAllAsArray();
        return new JsonModel(array('data' => $data));
    }

    public function getRolesAction() {
        $data = $this->entityManager->getRepository('\Staff\Entity\Role')->findAllAsArray();
        return new JsonModel(array('data' => $data));
    }

    public function addPostAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $hydrator = new DoctrineHydrator($this->entityManager, '\Staff\Entity\Post');
            $object = new \Staff\Entity\Post();
            $hydrator->hydrate($data, $object);
            $this->entityManager->persist($object);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }

    public function addSeparationAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $hydrator = new DoctrineHydrator($this->entityManager, '\Staff\Entity\Separation');
            $object = new \Staff\Entity\Separation();
            $hydrator->hydrate($data, $object);
            $this->entityManager->persist($object);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }

    public function addWorkplaceAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $hydrator = new DoctrineHydrator($this->entityManager, '\Staff\Entity\Workplace');
            $object = new \Staff\Entity\Workplace();
            $hydrator->hydrate($data, $object);
            $this->entityManager->persist($object);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }

    public function updateSeparationAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $hydrator = new DoctrineHydrator($this->entityManager, '\Staff\Entity\Separation');
            $object = $hydrator->hydrate($data, new \Staff\Entity\Separation());
            $this->entityManager->merge($object);
            $this->entityManager->persist($object);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }
    
    public function updatePostAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $hydrator = new DoctrineHydrator($this->entityManager, '\Staff\Entity\Post');
            $object = $hydrator->hydrate($data, new \Staff\Entity\Post());
            $this->entityManager->merge($object);
            $this->entityManager->persist($object);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }
    
    public function removePostAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $id = (int) $request->getPost('id');
            $object = $this->entityManager->getRepository('\Staff\Entity\Post')
                    ->findOneBy(array('id' => $id));
            $this->entityManager->remove($object);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }

    public function removeSeparationAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $id = (int) $request->getPost('id');
            $object = $this->entityManager->getRepository('\Staff\Entity\Separation')
                    ->findOneBy(array('id' => $id));
            $this->entityManager->remove($object);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }

    public function removeWorkplaceAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $id = (int) $request->getPost('id');
            $object = $this->entityManager->getRepository('\Staff\Entity\Workplace')
                    ->findOneBy(array('id' => $id));
            $this->entityManager->remove($object);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }

    public function setEntityManager($entityManager) {
        $this->entityManager = $entityManager;
    }

}
