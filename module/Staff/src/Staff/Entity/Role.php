<?php

namespace Staff\Entity;

use BjyAuthorize\Acl\HierarchicalRoleInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Role
 *
 * @ORM\Table(name="role")
 * @ORM\Entity(repositoryClass="Staff\Repository\RoleRepository")
 */
class Role implements HierarchicalRoleInterface {

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="role_title", type="string", length=45, unique=true, nullable=true)
     */
    private $roleTitle;

    /**
     * @var Role
     * @ORM\ManyToOne(targetEntity="Staff\Entity\Role")
     */
    private $parent;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Staff", mappedBy="role")
     */
    private $staff;

    public function __construct() {
        $this->staff = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = (int) $id;
    }

    public function getRoleTitle() {
        return $this->roleTitle;
    }

    public function setRoleTitle($roleTitle) {
        $this->roleTitle = (string) $roleTitle;
    }

    public function getParent() {
        return $this->parent;
    }

    public function setParent(Role $parent) {
        $this->parent = $parent;
    }

    public function getStaff() {
        return $this->staff;
    }

    public function setStaff($staff) {
        $this->staff = $staff;
    }

    // interface implements
    public function getRoleId() {
        return $this->getRoleTitle();
    }

    public function setRoleId($roleId) {
        $this->setRoleTitle($roleId);
    }

}
