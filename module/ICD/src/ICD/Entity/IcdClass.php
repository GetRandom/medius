<?php

namespace ICD\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IcdClass
 *
 * @ORM\Table(name="icd_class", uniqueConstraints={@ORM\UniqueConstraint(name="idClasses_UNIQUE", columns={"id"})})
 * @ORM\Entity
 */
class IcdClass
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    private $title;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return IcdClass
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }
}
