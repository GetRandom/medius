<?php

namespace ICD\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class IcdController extends AbstractActionController {

    protected $entityManager;

    public function allAction() {
        $icdClassQuery = $this->entityManager->createQuery('SELECT ic FROM ICD\Entity\IcdClass ic');
        $result = $icdClassQuery->getArrayResult();
        return new JsonModel($result);
    }
    
    public function allBlocksAction() {
        $icdClassQuery = $this->entityManager->createQuery('SELECT b FROM ICD\Entity\Block b');
        $data = $icdClassQuery->getArrayResult();
        return new JsonModel($data);
    }

    public function classAction() {
        $classId = $this->params('id', null);
        $blockQuery = $this->entityManager->createQuery('SELECT b FROM ICD\Entity\Block b WHERE b.icdClass = ?1');
        $blockQuery->setParameter(1, $classId);
        $result = $blockQuery->getArrayResult();
        return new JsonModel($result);
    }

    public function allDiagnosisAction() {
        $icdClassQuery = $this->entityManager->createQuery('SELECT d FROM ICD\Entity\Diagnosis d');
        $data = $icdClassQuery->getArrayResult();
        return new JsonModel($data);
    }
    
    public function blockAction() {
        $blockId = $this->params('id', null);
        $diagnosisQuery = $this->entityManager->createQuery('SELECT d FROM ICD\Entity\Diagnosis d WHERE d.block = :block');
        $diagnosisQuery->setParameter('block', $blockId);
        $result = $diagnosisQuery->getArrayResult();
        return new JsonModel($result);
    }

    public function searchAction() {
        $id = $this->params('id', null);
        $data = array();
        if (iconv_strlen($id, 'UTF-8') > 2) {
            $expression = '%' . $id . '%';
            
            $classQuery = $this->entityManager->createQuery('SELECT c FROM ICD\Entity\IcdClass c WHERE c.title LIKE :expr');
            $classQuery->setParameter('expr', $expression);

            $blockQuery = $this->entityManager->createQuery('SELECT b FROM ICD\Entity\Block b WHERE b.title LIKE :expr');
            $blockQuery->setParameter('expr', $expression);

            $diagnosisQuery = $this->entityManager->createQuery('SELECT d FROM ICD\Entity\Diagnosis d '
                    . 'WHERE d.title LIKE :expr OR d.number LIKE :expr');
            $diagnosisQuery->setParameter('expr', $expression);

            $data['class'] = $classQuery->getArrayResult();
            $data['block'] = $blockQuery->getArrayResult();
            $data['diagnosis'] = $diagnosisQuery->getArrayResult();
        }
        return new JsonModel($data);
    }

    public function setEntityManager($entityManager) {
        $this->entityManager = $entityManager;
    }

}
