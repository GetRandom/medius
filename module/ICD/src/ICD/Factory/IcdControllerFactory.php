<?php

namespace ICD\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use ICD\Controller\IcdController;

class IcdControllerFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {
        $controller = new IcdController();
        $controller->setEntityManager(
                $serviceLocator->getServiceLocator()->get('Doctrine\ORM\EntityManager')
        );
        return $controller;
    }

}
