<?php

namespace Common\Controller;

use Zend\View\Model\JsonModel;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class MedcardController extends AbstractMediusController {

    const ENTITY_NAME = '\Common\Entity\Medcard';

// PUBLIC:
    public function allAction() {
        $data = $this->entityManager->createQuery('SELECT m FROM ' . self::ENTITY_NAME . ' m')->getArrayResult();
        return new JsonModel($data);
    }

    public function viewAction() {
        $id = $this->params()->fromRoute('id');
        $data = $this->repository->findByIdAsArray($id);
        return new JsonModel($data);
    }

    public function addAction() {
        if ($this->getRequest()->isPost()) {
            $data = $this->getJsonContent();
            $medcard = new \Common\Entity\Medcard();
            $hospitalization = new \Medcard\Entity\Hospitalization();
            $patient = new \Medcard\Entity\Patient();
            $location = new \Medcard\Entity\Location();
            $treatment = new \Medcard\Entity\Treatment();
            $admissionDepartment = new \Medcard\Entity\AdmissionDepartment();
            $treatmentDiagnosis = new \Medcard\Entity\TreatmentDiagnosis();

            $medcard->setNumber($data['number']);
            $hospitalization->setMedcard($medcard);
            $patient->setMedcard($medcard);
            $location->setPatient($patient);
            $treatment->setMedcard($medcard);
            $admissionDepartment->setTreatment($treatment);
            $treatmentDiagnosis->setTreatment($treatment);

            $this->entityManager->persist($medcard);
            $this->entityManager->persist($hospitalization);
            $this->entityManager->persist($patient);
            $this->entityManager->persist($location);
            $this->entityManager->persist($treatment);
            $this->entityManager->persist($admissionDepartment);
            $this->entityManager->persist($treatmentDiagnosis);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }

    public function updateAction() {
        if ($this->getRequest()->isPost()) {
            $data = $this->getJsonContent();
            if ($data['food'] == NULL) {
                unset($data['food']);
            }
            if ($data['separation'] == NULL) {
                unset($data['separation']);
            }
            $medcard = $this->hydrator->hydrate($data, new \Common\Entity\Medcard());
            $this->entityManager->merge($medcard);
            $this->entityManager->persist($medcard);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }

    public function removeAction() {
        if ($this->getRequest()->isPost()) {
            $id = $this->getJsonContent()['id'];
            $medcard = $this->entityManager->getRepository(self::ENTITY_NAME)
                    ->findOneBy(array('id' => $id));
            $this->entityManager->remove($medcard);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }

}
