<?php

namespace Common\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Medcard
 *
 * @ORM\Table(name="medcard", uniqueConstraints={@ORM\UniqueConstraint(name="number_UNIQUE", columns={"number"})})
 * @ORM\Entity(repositoryClass="\Common\Repository\MedcardRepository")
 */
class Medcard
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=45, nullable=false)
     */
    private $number;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="hosp_date", type="datetime", nullable=true)
     */
    private $hospDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="chamber", type="integer", nullable=true)
     */
    private $chamber;

    /**
     * @var string
     *
     * @ORM\Column(name="current_year_hosp", type="string", nullable=true)
     */
    private $currentYearHosp;

    /**
     * @var string
     *
     * @ORM\Column(name="sender", type="string", length=100, nullable=true)
     */
    private $sender;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pediculosis", type="boolean", nullable=true)
     */
    private $pediculosis;

    /**
     * @var boolean
     *
     * @ORM\Column(name="scab_inspection", type="boolean", nullable=true)
     */
    private $scabInspection;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hepatitis", type="boolean", nullable=true)
     */
    private $hepatitis;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="pulse_value", type="integer", nullable=true)
     */
    private $pulseValue;

    /**
     * @var float
     *
     * @ORM\Column(name="temperature_value", type="float", precision=10, scale=0, nullable=true)
     */
    private $temperatureValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="pressure_top", type="integer", nullable=true)
     */
    private $pressureTop;

    /**
     * @var integer
     *
     * @ORM\Column(name="pressure_lower", type="integer", nullable=true)
     */
    private $pressureLower;

    /**
     * @var \Medcard\Entity\Food
     *
     * @ORM\ManyToOne(targetEntity="Medcard\Entity\Food")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="food_id", referencedColumnName="id")
     * })
     */
    private $food;

    /**
     * @ORM\OneToOne(targetEntity="Medcard\Entity\Patient", mappedBy="medcard")
     */
    private $patient;

    /**
     * @ORM\OneToOne(targetEntity="Medcard\Entity\Hospitalization", mappedBy="medcard")
     */
    private $hospitalization;
    
    /**
     * @ORM\OneToOne(targetEntity="Medcard\Entity\Treatment", mappedBy="medcard")
     */
    private $treatment;
    
    /**
     * @var \Staff\Entity\Separation
     *
     * @ORM\ManyToOne(targetEntity="Staff\Entity\Separation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="separation_id", referencedColumnName="id")
     * })
     */
    private $separation;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return Medcard
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set hospDate
     *
     * @param \DateTime $hospDate
     * @return Medcard
     */
    public function setHospDate($hospDate)
    {
        $this->hospDate = $hospDate;

        return $this;
    }

    /**
     * Get hospDate
     *
     * @return \DateTime 
     */
    public function getHospDate()
    {
        return $this->hospDate;
    }

    /**
     * Set chamber
     *
     * @param integer $chamber
     * @return Medcard
     */
    public function setChamber($chamber)
    {
        $this->chamber = $chamber;

        return $this;
    }

    /**
     * Get chamber
     *
     * @return integer 
     */
    public function getChamber()
    {
        return $this->chamber;
    }

    /**
     * Set currentYearHosp
     *
     * @param string $currentYearHosp
     * @return Medcard
     */
    public function setCurrentYearHosp($currentYearHosp)
    {
        $this->currentYearHosp = $currentYearHosp;

        return $this;
    }

    /**
     * Get currentYearHosp
     *
     * @return string 
     */
    public function getCurrentYearHosp()
    {
        return $this->currentYearHosp;
    }

    /**
     * Set sender
     *
     * @param string $sender
     * @return Medcard
     */
    public function setSender($sender)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender
     *
     * @return string 
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set pediculosis
     *
     * @param boolean $pediculosis
     * @return Medcard
     */
    public function setPediculosis($pediculosis)
    {
        $this->pediculosis = $pediculosis;

        return $this;
    }

    /**
     * Get pediculosis
     *
     * @return boolean 
     */
    public function getPediculosis()
    {
        return $this->pediculosis;
    }

    /**
     * Set scabInspection
     *
     * @param boolean $scabInspection
     * @return Medcard
     */
    public function setScabInspection($scabInspection)
    {
        $this->scabInspection = $scabInspection;

        return $this;
    }

    /**
     * Get scabInspection
     *
     * @return boolean 
     */
    public function getScabInspection()
    {
        return $this->scabInspection;
    }

    /**
     * Set hepatitis
     *
     * @param boolean $hepatitis
     * @return Medcard
     */
    public function setHepatitis($hepatitis)
    {
        $this->hepatitis = $hepatitis;

        return $this;
    }

    /**
     * Get hepatitis
     *
     * @return boolean 
     */
    public function getHepatitis()
    {
        return $this->hepatitis;
    }

    /**
     * Set food
     *
     * @param \Medcard\Entity\Food $food
     * @return Medcard
     */
    public function setFood(\Medcard\Entity\Food $food = null)
    {
        $this->food = $food;

        return $this;
    }

    /**
     * Get food
     *
     * @return \Medcard\Entity\Food 
     */
    public function getFood()
    {
        return $this->food;
    }

    /**
     * Set patient
     *
     * @param \Medcard\Entity\Patient $patient
     * @return Medcard
     */
    public function setPatient(\Medcard\Entity\Patient $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient
     *
     * @return \Medcard\Entity\Patient 
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * Set hospitalization
     *
     * @param \Medcard\Entity\Hospitalization $hospitalization
     * @return Medcard
     */
    public function setHospitalization(\Medcard\Entity\Hospitalization $hospitalization = null)
    {
        $this->hospitalization = $hospitalization;

        return $this;
    }

    /**
     * Get hospitalization
     *
     * @return \Medcard\Entity\Hospitalization 
     */
    public function getHospitalization()
    {
        return $this->hospitalization;
    }

    /**
     * Set treatment
     *
     * @param \Medcard\Entity\Treatment $treatment
     * @return Medcard
     */
    public function setTreatment(\Medcard\Entity\Treatment $treatment = null)
    {
        $this->treatment = $treatment;

        return $this;
    }

    /**
     * Get treatment
     *
     * @return \Medcard\Entity\Treatment 
     */
    public function getTreatment()
    {
        return $this->treatment;
    }
    
    /**
     * Set separation
     *
     * @param string $separation
     * @return Medcard
     */
    public function setSeparation($separation)
    {
        $this->separation = $separation;

        return $this;
    }

    /**
     * Get separation
     *
     * @return string 
     */
    public function getSeparation()
    {
        return $this->separation;
    }

    /**
     * Set pulseValue
     *
     * @param integer $pulseValue
     * @return Medcard
     */
    public function setPulseValue($pulseValue)
    {
        $this->pulseValue = $pulseValue;

        return $this;
    }

    /**
     * Get pulseValue
     *
     * @return integer 
     */
    public function getPulseValue()
    {
        return $this->pulseValue;
    }

    /**
     * Set temperatureValue
     *
     * @param float $temperatureValue
     * @return Medcard
     */
    public function setTemperatureValue($temperatureValue)
    {
        $this->temperatureValue = $temperatureValue;

        return $this;
    }

    /**
     * Get temperatureValue
     *
     * @return float 
     */
    public function getTemperatureValue()
    {
        return $this->temperatureValue;
    }

    /**
     * Set pressureTop
     *
     * @param integer $pressureTop
     * @return Medcard
     */
    public function setPressureTop($pressureTop)
    {
        $this->pressureTop = $pressureTop;

        return $this;
    }

    /**
     * Get pressureTop
     *
     * @return integer 
     */
    public function getPressureTop()
    {
        return $this->pressureTop;
    }

    /**
     * Set pressureLower
     *
     * @param integer $pressureLower
     * @return Medcard
     */
    public function setPressureLower($pressureLower)
    {
        $this->pressureLower = $pressureLower;

        return $this;
    }

    /**
     * Get pressureLower
     *
     * @return integer 
     */
    public function getPressureLower()
    {
        return $this->pressureLower;
    }
}
