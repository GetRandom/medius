<?php

namespace Common\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Common\Controller\MedcardController;

class MedcardControllerFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {
        $entityName = '\Common\Entity\Medcard';
        $entityManager = $serviceLocator->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $repository = $entityManager->getRepository($entityName);
        $hydrator = new DoctrineHydrator($entityManager, $entityName);
        
        $controller = new MedcardController();
        $controller->setEntityManager($entityManager);
        $controller->setRepository($repository);
        $controller->setHydrator($hydrator);
        
        return $controller;
    }

}
