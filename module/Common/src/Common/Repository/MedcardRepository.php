<?php

namespace Common\Repository;

use Doctrine\ORM\EntityRepository;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class MedcardRepository extends EntityRepository {

    public function __construct($em, \Doctrine\ORM\Mapping\ClassMetadata $class) {
        parent::__construct($em, $class);
    }

    public function findByIdAsArray($id) {
        $data = $this->_em->createQuery('SELECT m FROM ' . $this->_entityName . ' m WHERE m.number = ?1')
                        ->setParameter(1, $id)->getSingleResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
        $object = $this->findOneBy(array('number' => $id));
        $data['food'] = $object->getFood() ? $object->getFood()->getId() : null;
        $data['separation'] = $object->getSeparation() ? $object->getSeparation()->getId() : null;
        return $data;
    }

}
