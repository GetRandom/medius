<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IcdController extends AbstractActionController {

    public function indexAction() {
        return new ViewModel();
    }

    public function classAction() {
        return new ViewModel();
    }
    
    public function blockAction() {
        return new ViewModel();
    }
    
    public function searchAction() {
        return new ViewModel();
    }
}
