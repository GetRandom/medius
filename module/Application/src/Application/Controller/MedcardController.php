<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class MedcardController extends AbstractActionController {

    public function indexAction() {
        return new ViewModel();
    }

    public function addAction() {
        return new ViewModel();
    }

    public function viewAction() {
        return new ViewModel();
    }
    
    public function printAction() {
        return (new ViewModel())->setTerminal(true);
    }
}
