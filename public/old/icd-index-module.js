function IcdIndexModule() {
    var allClassesGetter = $.Deferred();
    var allClassesGetterPromise = allClassesGetter.pipe(med.icd.all);

    this.start = function () {
        $("#search").keyup(function (event) {
            if (event.keyCode === 13) {
                $("#search").click(function (event) {
                    window.location.href = '/icd/search/' + event.target.value;
                });
                $("#search").click();
            }
        });


        allClassesGetter.resolve();
        $("#classesList").css({"visibility": "hidden"});
        staticContentOn();
    };

    var successIcdGetter = function (result) {
        $('#classesList').append(getIcdClassesHtml(result));
        $('.preloaderbg').hide();
        $("#classesList").css({"visibility": "visible"});
        staticContentOff();
    };

    allClassesGetterPromise.done(successIcdGetter);
}
;
window.icdIndexModule = new IcdIndexModule();