$(document).ready(function () {
    $('#logout').click(function (event) {
        med.staff.auth.logout();
        window.location = '/staff/login';
        return false;
    });

    showPassword();
});

function enabledInputs() {
    $("#edit").click(function () {
        $("fieldset").prop("disabled", false);
        $("select").removeClass("form-control");
    });
}

function linkOnRow() {
    jQuery(function ($) {
        $('tbody tr[data-href]').addClass('clickable').click(function () {
            window.location = $(this).attr('data-href');
        }).find('a').hover(function () {
            $(this).parents('tr').unbind('click');
        }, function () {
            $(this).parents('tr').click(function () {
                window.location = $(this).attr('data-href');
            });
        });
    });
}

function showPassword() {
    $(".showPass").click(function () {
        var attrIn = $(".innerL input:nth-child(1)").attr("type");
        if (attrIn === "password") {
            $(".innerL input:nth-child(1)").attr("type", "text").css({"color": "#A7A7A7"});
            $(".fa-eye").removeClass("fa-eye").addClass("fa-eye-slash");
        }
        else {
            $(".innerL input:nth-child(1)").attr("type", "password").css({"color": "#000"});
            $(".fa-eye-slash").removeClass("fa-eye-slash").addClass("fa-eye");
        }
    });
}

function staticContentOn() {
    $(".widget-body").addClass("staticContent");
}

function staticContentOff() {
    $(".widget-body").removeClass("staticContent");
}

function confMenu() {
    var getCookie = function (name) {
        var matches = document.cookie.match(new RegExp(
                "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
                ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    };

    var role = getCookie('role');

    if (role !== 'administrator') {
        $('#staffMenu').hide();// = true;
        $('#staffMenuItem').hide();// = true;
        $('#adminMenu').hide();// = true;
        $('#adminMenuItem').hide();// = true;
    }
    if (role === 'nurse') {
        $('#MedcardMenuAdd').hide();
    }
};

$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});