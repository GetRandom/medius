function StaffIndexModule() {
    var allStaffGetter = $.Deferred();
    var allStaffGetterPromise = allStaffGetter.pipe(med.staff.staff.all);
    

    this.start = function() {
        staticContentOn();
        allStaffGetter.resolve(0, 100); // hardcode
    };

    var successStaffGetter = function(result) {
        $('#dataTableBody').html(tableBodyFilling(result.data));
        staticContentOff();
        $('.preloaderbg').hide();
        $("table").css({"visibility": "visible"});
    };
    var alwaysStaffGetter = function() {
        $(".alert").addClass("in").fadeOut(4500);
        $("#DataTables_Table_0").DataTable();
        linkOnRow();
    };

    allStaffGetterPromise.done(successStaffGetter);
    allStaffGetterPromise.always(alwaysStaffGetter);
}
;
window.staffIndexModule = new StaffIndexModule();