function StaffAdditionModule() {
    var workplaceGetting = new $.Deferred();
    var separationGetting = new $.Deferred();
    var postGetting = new $.Deferred();
    var roleGetting = new $.Deferred();
    var staffAddition = new $.Deferred();

    var workplaceGettingPromise = workplaceGetting.pipe(med.staff.digest.getWorkplaces);
    var separationGettingPromise = separationGetting.pipe(med.staff.digest.getSeparations);
    var postGettingPromise = postGetting.pipe(med.staff.digest.getPosts);
    var roleGettingPromise = roleGetting.pipe(med.staff.digest.getRoles);
    var staffAdditionPromise = staffAddition.pipe(med.staff.staff.add);

    this.start = function() {
        $("form").css({"visibility": "hidden"});
        workplaceGetting.resolve();
        separationGetting.resolve();
        postGetting.resolve();
        roleGetting.resolve();
    };

    // Common handlers
    var failRequestHandler = function(result) {
        console.log(result);
    };
    var alwaysRequestHandler = function() {
        /* Initialization "Chosen plugin" - must be last action with HTML doc. */
        var config = {
            '.chosen-select': {},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        };
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    };
    var submitHandler = function(eventObject) {
        if (true) { // Validation result will be here
            staffAddition.resolve(formToJson($(eventObject.target)));
        }
        else {
            staffAddition.reject('some errorMsg');
        }
        return false;
    };
    var successPassGenerationHandler = function(result) {
        $('#pass3')[0].value = result.password;
    };
    var passGenButtonHandler = function(eventObject) {
        $.get('/api/staff/pass-gen', successPassGenerationHandler);
        return false;
    };

    // Addition staff
    var successStaffAdditionHandler = function(result) {
        window.location = '/staff';
        alert('[OK]');
    };

    // Get digests
    var successWorkplaceGettingHandler = function(result) {
        $('#workplaces_id').append(optionListFilling(result.data));
    };
    var successSeparationGettingHandler = function(result) {
        $('#separations_id').append(optionListFilling(result.data));
    };
    var successPostGettingHandler = function(result) {
        $('#posts_id').append(optionListFilling(result.data));
    };
    var successRoleGettingHandler = function(result) {
        $('#roles_id').append(optionListFilling(result.data));
    };
    var allDigestsReceivedHandler = function() {
        $('.preloaderbg').hide();
        $("form").css({"visibility": "visible"});
        $("#jForm").submit(submitHandler);
        $('#generate').click(passGenButtonHandler);
    };


    workplaceGettingPromise.done(successWorkplaceGettingHandler);

    separationGettingPromise.done(successSeparationGettingHandler);

    postGettingPromise.done(successPostGettingHandler);

    roleGettingPromise.done(successRoleGettingHandler);

    var allDigestsReceivedPromise = $.when(
            workplaceGettingPromise, separationGettingPromise,
            postGettingPromise, roleGettingPromise
            );

    allDigestsReceivedPromise.done(allDigestsReceivedHandler);
    allDigestsReceivedPromise.fail(failRequestHandler);
    allDigestsReceivedPromise.always(alwaysRequestHandler);

    staffAdditionPromise.done(successStaffAdditionHandler);
    staffAdditionPromise.fail(failRequestHandler);
}
;
window.staffAdditionModule = new StaffAdditionModule();