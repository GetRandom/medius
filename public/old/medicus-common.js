function jsDateToYmd(date) {
    if (date !== null) {
        if (date.date !== null) {
            date = (date.date.getFullYear() + '-' + (date.date.getMonth() + 1) + '-' + addZero(date.date.getDate())
                    + ' ' + date.date.getHours() + ':' + date.date.getMinutes());
        }
        else {
            date = null;
        }
    }
    return date;
}
;

function phpDateToJs(date) {
    if (date !== null) {
        date.date = new Date(date.date);
    }
    return date;
}
;

function calcAge(born) {
    var d = born.getDate();
    var m = born.getMonth();
    var g = born.getFullYear();
    //устанавливаем начальное время  и сегодняшнее
    var a = new Date(g, m - 1, d, 0, 0, 0, 0), b = new Date;
    b.setHours(0, 0, 0, 0);
    for (m = 0; ; m++) {
        g = new Date(a.getFullYear(), a.getMonth() + 2, 0);
        g.getDate() > d && g.setDate(d);
        if (g > b)
            break;
        a = g
    }
    d = b - a;//оставшееся время за последний неполный месяц, если будет
    d = Math.round(d / 864E5);//количество полных дней в этом времени
    g = Math.floor(m / 12);//сколько полных лет в подсчитанных месяцах
    m = m % 12;//оставшиеся месяцы от полных лет
    return g;
}
;


function addZero(i) {
    return (i < 10) ? "0" + i : i;
}
;

function formToJson(form) {
    var formData = form.serializeArray();
    var data = {};
    data.workplaces = [];
    data.separations = [];
    data.posts = [];
    data.roles = [];

    for (var i = 0; i < formData.length; i++) {
        if (formData[i].name === 'workplaces') {
            data.workplaces.push(formData[i].value);
        }
        else if (formData[i].name === 'separations') {
            data.separations.push(formData[i].value);
        }
        else if (formData[i].name === 'posts') {
            data.posts.push(formData[i].value);
        }
        else if (formData[i].name === 'roles') {
            data.roles.push(formData[i].value);
        }
        else {
            data[formData[i].name] = formData[i].value;
        }
    }
    return data;
}
;

function medcardFormToJson(form) {
    var formData = form.serializeArray();
    var data = {};

    data.patient = {};
    for (var i = 0; i < formData.length; i++) {
        if (formData[i].name === 'category' || formData[i].name === 'id') {
            data[formData[i].name] = formData[i].value;
        }
        else {
            data.patient[formData[i].name] = formData[i].value;
        }
    }
    return data;
}
;

function staffFormFilling(form, data) {
    form['id'].value = data.id;
    form['login'].value = data.login;
    form['password'].value = data.password;
    form['name'].value = data.name;
    form['lastname'].value = data.lastname;
    form['patronymic'].value = data.patronymic;

    for (var i = 0; i < data.workplaces.length; i++) {
        var opt = $('[value=' + data.workplaces[i].id + ']', form['workplaces'])[0];
        opt.selected = 'selected';
    }
    for (var i = 0; i < data.separations.length; i++) {
        var opt = $('[value=' + data.separations[i].id + ']', form['separations'])[0];
        opt.selected = 'selected';
    }
    for (var i = 0; i < data.posts.length; i++) {
        var opt = $('[value=' + data.posts[i].id + ']', form['posts'])[0];
        opt.selected = 'selected';
    }
    for (var i = 0; i < data.roles.length; i++) {
        var opt = $('[value=' + data.roles[i].id + ']', form['roles'])[0];
        opt.selected = 'selected';
    }
    $('#remove').prop('value', data.id);
}
;

function medcardFormFilling(form, data) {
    form['id'].value = data.id;
    form['name'].value = data.patient.name;
    form['lastname'].value = data.patient.lastname;
    form['patronymic'].value = data.patient.patronymic;
    form['insuranceCertificate'].value = data.patient.insuranceCertificate;
    form['address'].value = data.patient.address;
    form['area'].value = data.patient.area;
    form['region'].value = data.patient.region;
    var tmp = data.patient.born.split('-');
    var born = tmp[2] + '-' + tmp[1] + '-' + tmp[0];
    form['born'].value = born;
    form['bornPlace'].value = data.patient.bornPlace;
    form['phone'].value = data.patient.phone;
    form['sensitivity'].value = data.patient.sensitivity;
    $('[value=' + data.patient.bloodType + ']', form['bloodType'])[0].selected = 'selected';
    form['rhFactor'].value = data.patient.rhFactor;
    $('[value=' + data.category.id + ']', form['category'])[0].selected = 'selected';
    $('#remove').prop('value', data.id);
    $('#print').prop('value', data.id);
}
;

function optionListFilling(data) {
    console.log(data);
    var result = '';
    for (var i = 0; i < data.length; i++) {
        if ('title' in data[i]) {
            result += '<option value="' + data[i].id + '">' + data[i].title + '</option>';
        }
        else if ('role_title' in data[i]) {
            result += '<option value="' + data[i].id + '">' + data[i].role_title + '</option>';
        }
        else if ('roleTitle' in data[i]) {
            result += '<option value="' + data[i].id + '">' + data[i].roleTitle + '</option>';
        }
    }
    return result;
}
;

function tableBodyFilling(data) {
    var html = '';
    for (var i = 0; i < data.length; i++) {
        html += '<tr data-href="' + '/staff/view/' + data[i].id + '">';
        html += '<td>' + (i + 1) + '</td>';
        html += '<td>' + data[i].name + '</td>';
        html += '<td>' + data[i].lastname + '</td>';
        html += '<td>' + data[i].patronymic + '</td>';
        html += '<td>' + data[i].roles[0].roleTitle + '</td>';
        html += '</tr>';
    }
    return html;
}
;

function medcardTableFilling(data) {
    var html = '';
    for (var i = 0; i < data.length; i++) {
        html += '<tr data-href="' + '/medcard/view/' + data[i].id + '">';
        html += '<td><a href="#">' + data[i].id + '</a></td>';
        html += '<td>' + data[i].patient.name + '</td>';
        html += '<td>' + data[i].patient.lastname + '</td>';
        html += '<td>' + data[i].patient.patronymic + '</td>';
        html += '<td>' + data[i].category.title + '</td>';
        html += '</tr>';
    }
    return html;
}
;

function getIcdClassesHtml(data) {
    var html = '';
    for (var i = 0; i < data.length; i++) {
        html += '<h4><a href="' + '/icd/class/' + data[i].id + '"><div class="row"><div class="col-xs-1 text-right">'
                + data[i].id + '</div><div class="col-xs-11">' + data[i].title + '</div></div></a></h4>';
    }
    return html;
}
;

function getIcdBlocksHtml(data) {
    var html = '';
    for (var i = 0; i < data.length; i++) {
        html += '<h4><a href="' + '/icd/block/' + data[i].id + '"><div class="row"><div class="col-xs-1 text-right">'
                + data[i].id + '</div><div class="col-xs-11">' + data[i].title + '</div></div></a></h4>';
    }
    return html;
}
;

function getIcdDiagnosisHtml(data) {
    var html = '';
    for (var i = 0; i < data.length; i++) {
        html += '<h4><a><div class="row"><div class="col-xs-1 text-right">'
                + data[i].number + '</div><div class="col-xs-11">' + data[i].title + '</div></div></a></h4>';
    }
    return html;
}
;