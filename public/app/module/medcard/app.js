
angular.module('medicus', ['ngGrid', 'ui.bootstrap']);

//We already have a limitTo filter built-in to angular,
//let's make a startFrom filter
angular.module('medicus').filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        if (input.slice)
            return input.slice(start);
        else
            return null;
    };
});