function ExaminationResultController($scope, examinationResultApiFactory) {
    $scope.results = {};

    var editCellTemp = '<div class="pdn-5"><textarea rows="3" class="form-control" ng-class="\'colt\' + col.index" ng-input="COL_FIELD" ng-model="COL_FIELD" ></textarea></div>';
    var cellTemp = '<div ng-class="col.colIndex()"><div class="pdn-5" ng-cell-text>{{row.getProperty(col.field)}}</div></div>';

    $scope.resultsGrid = {
        data: 'results',
        enableCellSelection: true,
        enableCellEdit: true,
        enableRowSelection: false,
        rowHeight: 90,
        columnDefs: [
            {field: 'begin.date', displayName: 'Дата', width: 145, headerClass: 'text-center',
                cellTemplate: '<div class="pdn-5"><input class="form-control-treat" type="date" ng-model="row.entity.date.date"></div>'},
            {field: 'note', displayName: 'Нотатки', headerClass: 'text-center',
                editableCellTemplate: editCellTemp,
                cellTemplate: cellTemp},
            {width:75, cellTemplate: '<div class="text-center pdn-5"><button title="Зберегти" class="btn-treat btn-success" ng-click="updateExaminationResult(row.entity)"><i class="fa fa-save"></i></button>\n\
                                <button title="Стерти" class="btn-treat btn-danger" ng-click="removeExaminationResult(row.entity)"><i class="fa fa-eraser"></i></button></div>'}
        ]
    };

// HANDLERS:
    var allExaminationResultsObtained = function (data) {
        for (var i = 0; i < data.length; i++) {
            data[i].date = phpDateToJs(data[i].date);
        }
        data.push({title: null, date: null});
        $scope.results = data;
        console.log('allExaminationResultsObtained', data);
    };
    var examinationResultUpdated = function (data) {
        console.log('examinationResultUpdated', data);
        $scope.allExaminationResults($scope.medcard.treatment);
    };
    var examinationResultRemoved = function (data) {
        console.log('examinationResultRemoved', data);
        $scope.allExaminationResults($scope.medcard.treatment);
    };

// ACTIONS:
    $scope.$on('treatmentObtained', function (event, data) {
        $scope.allExaminationResults(data);
    });

    $scope.allExaminationResults = function (data) {
        var request = examinationResultApiFactory.all(data.id);
        request.success(allExaminationResultsObtained);
    };
    $scope.updateExaminationResult = function (data) {
        var note = {};
        angular.copy(data, note);
        note.date = jsDateToYmd(note.date);
        note.treatment = $scope.medcard.treatment.id;
        var request = examinationResultApiFactory.update(note);
        request.success(examinationResultUpdated);
    };

    $scope.removeExaminationResult = function (data) {
        var request = examinationResultApiFactory.remove(data);
        request.success(examinationResultRemoved);
    };

}
;