
function PatientController($scope, patientApiFactory) {

// HANDLERS:
    var allPatientsObtained = function (data) {
        for (var i = 0; i < $scope.medcards.length; i++) {
            for (var j = 0; j < data.length; j++) {
                if ($scope.medcards[i].id === data[j].medcard) {
                    $scope.medcards[i].patient = data[j];
                }
            }
        }
        $scope.$broadcast('allPatientsObtained', data);
        console.log('allPatientsObtained', data);
    };
    var patientObtained = function (data) {
        data.born = phpDateToJs(data.born);
        data.rw = phpDateToJs(data.rw);
        data.hivInfection = phpDateToJs(data.hivInfection);
        $scope.medcard.patient = data;
        $scope.$broadcast('patientObtained', data);
        console.log('patientObtained', data);
        
        $('.preloaderbg').hide();
        $("#medcard").css({"visibility": "visible"});
    };
    var patientUpdated = function (data) {
        console.log('patientUpdated', data);
    };
    
    var error = function(data) {
        console.log('error', data);
    };

// ACTIONS:
    $scope.$on('allMedcardsObtained', function (event, data) {
        $scope.allPatient();
    });
    $scope.$on('medcardObtained', function (event, data) {
        $scope.getPatient(data.id);
    });
    $scope.$on('medcardUpdating', function (event, data) {
        $scope.updatePatient(data);
    });

    $scope.allPatient = function () {
        var request = patientApiFactory.all();
        request.success(allPatientsObtained);
        request.error(error);
    };
    $scope.getPatient = function (id) {
        var request = patientApiFactory.view(id);
        request.success(patientObtained);
        request.error(error);
    };
    $scope.updatePatient = function (data) {
        var patient = {};
        angular.copy(data.patient, patient);
        patient.born = jsDateToYmd(patient.born);
        patient.rw = jsDateToYmd(patient.rw);
        patient.hivInfection = jsDateToYmd(patient.hivInfection);
        var request = patientApiFactory.update(patient);
        request.success(patientUpdated);
        request.error(error);
        $scope.$broadcast('patientUpdating', patient);
    };
}
;