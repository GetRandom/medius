function OperationController($scope, operationApiFactory) {
    $scope.operations = {};

    var editCellTemp = '<div class="pdn-5"><textarea class="form-control" ng-class="\'colt\' + col.index" ng-input="COL_FIELD" ng-model="COL_FIELD" ></textarea></div>';
    var cellTemp = '<div ng-class="col.colIndex()"><div class="pdn-5" ng-cell-text>{{row.getProperty(col.field)}}</div></div>';

    $scope.operationGrid = {
        data: 'operations',
        enableCellSelection: true,
        enableCellEdit: true,
        enableRowSelection: false,
        rowHeight: 70,
        columnDefs: [
            {field: 'number', displayName: '# з/п', width: 75, headerClass: 'text-center',
                editableCellTemplate: '<div class="mgn-5"><input type="text" class="form-control-treat" ng-class="\'colt\' + col.index" ng-input="COL_FIELD" ng-model="COL_FIELD" /></div>',
                cellTemplate: cellTemp},
            {field: 'title', displayName: 'Найменування операції', headerClass: 'text-center',
                editableCellTemplate: editCellTemp,
                cellTemplate: cellTemp},
            {field: 'date.date', displayName: 'Дата, години', width: 160, headerClass: 'text-center',
                cellTemplate: '<div class="mgn-5"><input class="form-control-treat" type="date" ng-model="row.entity.date.date"></div>'},
            {field: 'anesthesiaMethod', displayName: 'Метод знеболювання', headerClass: 'text-center',
                editableCellTemplate: editCellTemp,
                cellTemplate: cellTemp},
            {field: 'complications', displayName: 'Ускладнення', headerClass: 'text-center',
                editableCellTemplate: editCellTemp,
                cellTemplate: cellTemp},
            {field: 'surgeon', displayName: 'Хирург', headerClass: 'text-center',
                editableCellTemplate: editCellTemp,
                cellTemplate: cellTemp},
            {field: 'anesthetist', displayName: 'Анестезиолог', headerClass: 'text-center',
                editableCellTemplate: editCellTemp,
                cellTemplate: cellTemp},
            {width: 75, cellTemplate: '<div class="text-center pdn-5"><button title="Зберегти" class="btn-treat btn-success" ng-click="updateOperation(row.entity)"><i class="fa fa-save"></i></button>\n\
                                <button title="Стерти" class="btn-treat btn-danger" ng-click="removeOperation(row.entity)"><i class="fa fa-eraser"></i></button></div>'}
        ]
    };

// HANDLERS:
    var allOperationsObtained = function (data) {
        for (var i = 0; i < data.length; i++) {
            data[i].date = phpDateToJs(data[i].date);
        }
        data.push({number: null, title: null, date: null, anesthesiaMethod: null, complications: null, surgeon: null, anesthetist: null});
        $scope.operations = data;
        console.log('allOperationsObtained', data);
    };
    var operationUpdated = function (data) {
        console.log('operationUpdated', data);
        $scope.allOperation($scope.medcard.treatment);
    };
    var operationRemoved = function (data) {
        console.log('operationRemoved', data);
        $scope.allOperation($scope.medcard.treatment);
    };

    var error = function (data) {
        console.log('error', data);
    };

// ACTIONS:
    $scope.$on('treatmentObtained', function (event, data) {
        $scope.allOperation(data);
    });

    $scope.allOperation = function (data) {
        var request = operationApiFactory.all(data.id);
        request.success(allOperationsObtained);
        request.error(error);
    };
    $scope.updateOperation = function (data) {
        var operation = {};
        angular.copy(data, operation);
        operation.date = jsDateToYmd(operation.date);
        operation.treatment = $scope.medcard.treatment.id;
        var request = operationApiFactory.update(operation);
        request.success(operationUpdated);
        request.error(error);
    };
    $scope.removeOperation = function (data) {
        var request = operationApiFactory.remove(data);
        request.success(operationRemoved);
        request.error(error);
    };

}
;