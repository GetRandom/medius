function AdmissionDepartmentController($scope, admissionDepartmentApiFactory) {

// HANDLERS:
    var admissionDepartmentObtained = function (data) {
        $scope.medcard.treatment.admissionDepartment = data;
        console.log('admissionDepartmentObtained', data);
    };
    var admissionDepartmentUpdated = function (data) {
        console.log('admissionDepartmentUpdated', data);
    };

    var error = function (data) {
        console.log('error', data);
    };

// ACTIONS:
    $scope.$on('treatmentObtained', function (event, data) {
        $scope.getAdmissionDepartment(data);
    });

    $scope.getAdmissionDepartment = function (data) {
        var request = admissionDepartmentApiFactory.view(data.id);
        request.success(admissionDepartmentObtained);
        request.error(error);
    };
    $scope.updateAdmissionDepartment = function (data) {
        var admissionDepartment = {};
        angular.copy(data, admissionDepartment);
        var request = admissionDepartmentApiFactory.update(admissionDepartment);
        request.success(admissionDepartmentUpdated);
        request.error(error);
    };

}
;