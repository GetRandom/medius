function EfficiencyMarkController($scope, efficiencyMarkApiFactory) {
    $scope.efficiencyMarks = {};

    $scope.efficiencyMarksGrid = {
        data: 'efficiencyMarks',
        enableCellSelection: true,
        enableCellEdit: true,
        enableRowSelection: false,
        rowHeight: 38,
        columnDefs: [
            {field: 'number', displayName: '# з/п', headerClass: 'text-center',
                editableCellTemplate: '<div><input type="text" class="form-control-treat" ng-class="\'colt\' + col.index" ng-input="COL_FIELD" ng-model="COL_FIELD" /></div>',
                cellTemplate: '<div ng-class="col.colIndex()"><div class="pdn-5" ng-cell-text>{{row.getProperty(col.field)}}</div></div>'},
            {field: 'begin.date', displayName: 'З', headerClass: 'text-center',
                cellTemplate: '<div class="pdn-5"><input class="form-control-treat" type="date" ng-model="row.entity.begin.date"></div>'},
            {field: 'end.date', displayName: 'По', headerClass: 'text-center',
                cellTemplate: '<div class="pdn-5"><input class="form-control-treat" type="date" ng-model="row.entity.end.date"></div>'},
            {width: 75, cellTemplate: '<div class="pdn-5"><button title="Зберегти" class="btn-treat btn-success" ng-click="updateEfficiencyMark(row.entity)"><i class="fa fa-save"></i></button>\n\
                                <button title="Стерти" class="btn-treat btn-danger" ng-click="removeEfficiencyMark(row.entity)"><i class="fa fa-eraser"></i></button></div>'}
        ]
    };

// HANDLERS:
    var allEfficiencyMarksObtained = function (data) {
        for (var i = 0; i < data.length; i++) {
            data[i].begin = phpDateToJs(data[i].begin);
            data[i].end = phpDateToJs(data[i].end);
        }
        data.push({number: null, begin: null, end: null});
        $scope.efficiencyMarks = data;
        console.log('allEfficiencyMarksObtained', data);
    };
    var efficiencyMarkUpdated = function (data) {
        console.log('efficiencyMarkUpdated', data);
        $scope.allEfficiencyMarks($scope.medcard.treatment);
    };
    var efficiencyMarkRemoved = function (data) {
        console.log('efficiencyMarkRemoved', data);
        $scope.allEfficiencyMarks($scope.medcard.treatment);
    };

// ACTIONS:
    $scope.$on('treatmentObtained', function (event, data) {
        $scope.allEfficiencyMarks(data);
    });

    $scope.allEfficiencyMarks = function (data) {
        var request = efficiencyMarkApiFactory.all(data.id);
        request.success(allEfficiencyMarksObtained);
    };
    $scope.updateEfficiencyMark = function (data) {
        var mark = {};
        angular.copy(data, mark);
        mark.begin = jsDateToYmd(mark.begin);
        mark.end = jsDateToYmd(mark.end);
        mark.treatment = $scope.medcard.treatment.id;
        var request = efficiencyMarkApiFactory.update(mark);
        request.success(efficiencyMarkUpdated);
    };

    $scope.removeEfficiencyMark = function (data) {
        var request = efficiencyMarkApiFactory.remove(data);
        request.success(efficiencyMarkRemoved);
    };

}
;