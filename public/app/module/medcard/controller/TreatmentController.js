function TreatmentController($rootScope, $scope, treatmentApiFactory) {

// HANDLERS:
    var treatmentObtained = function (data) {
        data.oncologicalObserving = phpDateToJs(data.oncologicalObserving);
        data.xrayObserving = phpDateToJs(data.xrayObserving);
        $scope.medcard.treatment = data;
        $rootScope.$broadcast('treatmentObtained', data);
        console.log('treatmentObtained', data);
    };
    var treatmentUpdated = function (data) {
        console.log('treatmentUpdated', data);
    };

    var error = function (data) {
        console.log('error', data);
    };

// ACTIONS:
    $scope.$on('medcardObtained', function (event, data) {
        $scope.getTreatment(data);
    });

    $scope.getTreatment = function (data) {
        var request = treatmentApiFactory.view(data.id);
        request.success(treatmentObtained);
        request.error(error);
    };
    $scope.updateTreatment = function (data) {
        $scope.$broadcast('treatmentUpdating', data);
        var treatment = {};
        angular.copy(data, treatment);
        delete treatment.operations;
        treatment.oncologicalObserving = jsDateToYmd(treatment.oncologicalObserving);
        treatment.xrayObserving = jsDateToYmd(treatment.xrayObserving);
        var request = treatmentApiFactory.update(treatment);
        request.success(treatmentUpdated);
        request.error(error);
    };
}
;