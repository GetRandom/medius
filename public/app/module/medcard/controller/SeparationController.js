function SeparationController($scope, separationApiFactory) {

    var separationButtonsTemplate = '<div class="categoryBT"><button class="btn-override btn-success" ng-click="updateSeparation(row.entity)"><i class="fa fa-check"> Оновити</i></button>'
            + '<button class="btn-override btn-danger" ng-click="removeSeparation(row.entity)"><i class="fa fa-remove"> Видалити</i></button></div>';
    $scope.separationGrid = {
        data: 'separations',
        enableCellSelection: true,
        enableCellEdit: true,
        enableRowSelection: false,
        columnDefs: [
            {field: 'title', displayName: 'Назва', headerClass: 'text-center', enableCellEdit: true},
            {cellClass:'text-center', cellTemplate: separationButtonsTemplate}
        ]
    };
    $scope.newSeparation = {title: null};


    $scope.allSeparation = function () {
        var request = separationApiFactory.all();
        var success = function (data) {
            $scope.separations = data.data;
        };
        request.success(success);
    };

    $scope.addSeparation = function (separation) {
        var request = separationApiFactory.add(separation);
        var success = function (data) {
            console.log(data);
            $scope.allSeparation();
            $scope.newSeparation = null;
        };
        request.success(success);
        request.error(success);
    };

    $scope.updateSeparation = function (separation) {
        var request = separationApiFactory.update(separation);
        var success = function (data) {
            console.log(data);
            $scope.allSeparation();
        };
        request.success(success);
    };

    $scope.removeSeparation = function (separation) {
        var request = separationApiFactory.remove(separation);
        var success = function (data) {
            console.log(data);
            $scope.allSeparation();
        };
        request.success(success);
    };
}
;