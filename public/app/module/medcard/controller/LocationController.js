function LocationController($scope, locationApiFactory, localityApiFactory, regionApiFactory) {
// HANDLERS:
    var locationObtained = function (data) {
        $scope.medcard.patient.location = data;
        $scope.getLocality(data);
        console.log('locationObtained', data);
    };
    var locationUpdated = function (data) {
        console.log('locationUpdated', data);
    };

    var allRegionsObtained = function (data) {
        $scope.regions = data;
        console.log('allRegionsObtained', data);
    };
    var allLocalitiesObtained = function (data) {
        $scope.localities = data;
        console.log('allLocalitiesObtained', data);
    };
    var localityObtained = function (data) {
        $scope.medcard.patient.location.locality = data.id;
        console.log('localityObtained', data);
        $scope.reg = data.region;
        $scope.getLocalities(data.region);
    };

    var error = function (data) {
        console.log('error', data);
    };

// ACTIONS:
    $scope.$on('patientObtained', function (event, data) {
        $scope.getLocation(data);
    });
    $scope.$on('patientUpdating', function (event, data) {
        $scope.updateLocation(data.location);
    });

    $scope.getLocation = function (data) {
        var request = locationApiFactory.view(data.id);
        request.success(locationObtained);
        request.error(error);
    };
    $scope.updateLocation = function (data) {
        var location = {};
        angular.copy(data, location);
        var request = locationApiFactory.update(location);
        request.success(locationUpdated);
        request.error(error);
    };

    $scope.getRegions = function () {
        var request = regionApiFactory.all();
        request.success(allRegionsObtained);
        request.error(error);
    };
    $scope.getLocalities = function (id) {
        var request = localityApiFactory.all(id);
        request.success(allLocalitiesObtained);
        request.error(error);
    };
    $scope.getLocality = function (data) {
        var request = localityApiFactory.view(data.locality);
        request.success(localityObtained);
        request.error(error);
    };
}
;