function PostController($scope, postApiFactory) {

    var postButtonsTemplate = '<div class="categoryBT"><button class="btn-override btn-success" ng-click="updatePost(row.entity)"><i class="fa fa-check"> Оновити</i></button>'
            + '<button class="btn-override btn-danger" ng-click="removePost(row.entity)"><i class="fa fa-remove"> Видалити</i></button></div>';
    $scope.postGrid = {
        data: 'posts',
        enableCellSelection: true,
        enableCellEdit: true,
        enableRowSelection: false,
        columnDefs: [
            {field: 'title', displayName: 'Назва', headerClass: 'text-center', enableCellEdit: true},
            {cellClass: 'text-center', cellTemplate: postButtonsTemplate}
        ]
    };
    $scope.newPost = {title: null};


    $scope.allPost = function () {
        var request = postApiFactory.all();
        var success = function (data) {
            $scope.posts = data.data;
        };
        request.success(success);
    };

    $scope.addPost = function (post) {
        var request = postApiFactory.add(post);
        var success = function (data) {
            console.log(data);
            $scope.allPost();
            $scope.newPost = null;
        };
        request.success(success);
        request.error(success);
    };

    $scope.updatePost = function (post) {
        var request = postApiFactory.update(post);
        var success = function (data) {
            console.log(data);
            $scope.allPost();
        };
        request.success(success);
    };

    $scope.removePost = function (post) {
        var request = postApiFactory.remove(post);
        var success = function (data) {
            console.log(data);
            $scope.allPost();
        };
        request.success(success);
    };
}
;