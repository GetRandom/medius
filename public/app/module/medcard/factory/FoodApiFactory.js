function foodApiFactory($http) {
    var factory = {};

    factory.all = function () {
        return $http.get('/api/food/all');
    };

    factory.view = function (id) {
        return $http.get('/api/food/view/' + id);
    };

    factory.add = function (data) {
        return $http.post('/api/food/add', data);
    };

    factory.update = function (data) {
        return $http.post('/api/food/update', data);
    };

    factory.remove = function (data) {
        return $http.post('/api/food/remove', data);
    };

    return factory;
}
;