function regionApiFactory($http) {
    var factory = {};

    factory.all = function () {
        return $http.get('/api/region/all');
    };

    return factory;
}
;