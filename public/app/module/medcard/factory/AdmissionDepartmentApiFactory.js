function admissionDepartmentApiFactory($http) {
    var factory = {};
    factory.view = function (id) {
        return $http.get('/api/admission-department/view/' + id);
    };
    factory.update = function (data) {
        return $http.post('/api/admission-department/update', data);
    };
    return factory;
}
;