function hospitalizationApiFactory($http) {
    var factory = {};
    factory.view = function (id) {
        return $http.get('/api/hospitalization/view/' + id);
    };
    factory.update = function (hospitalization) {
        return $http.post('/api/hospitalization/update', hospitalization);
    };
    return factory;
}
;