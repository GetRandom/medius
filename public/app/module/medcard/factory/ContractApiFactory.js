function contractApiFactory($http) {
    var factory = {};

    factory.all = function () {
        return $http.get('/api/contract/all');
    };

    factory.view = function (id) {
        return $http.get('/api/contract/view/' + id);
    };

    factory.add = function (data) {
        return $http.post('/api/contract/add', data);
    };

    factory.update = function (data) {
        return $http.post('/api/contract/update', data);
    };

    factory.remove = function (data) {
        return $http.post('/api/contract/remove', data);
    };

    return factory;
}
;