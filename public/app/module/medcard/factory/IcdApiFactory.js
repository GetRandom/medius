function icdApiFactory($http) {
    var factory = {};

    factory.all = function () {
        return $http.get('/api/icd/all');
    };

    factory.allBlocks = function () {
        return $http.get('/api/icd/all-blocks');
    };

    factory.allDiagnosis = function () {
        return $http.get('/api/icd/all-diagnosis');
    };

    factory.class = function (id) {
        return $http.get('/api/icd/class/' + id);
    };

    factory.block = function (id) {
        return $http.get('/api/icd/block/' + id);
    };

    factory.search = function (expr) {
        return $http.get('/api/icd/search/' + expr);
    };

    return factory;
}
;