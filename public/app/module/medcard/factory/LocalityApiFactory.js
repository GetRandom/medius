function localityApiFactory($http) {
    var factory = {};

    factory.all = function (id) {
        return $http.get('/api/locality/all/' + id);
    };

    factory.view = function (id) {
        return $http.get('/api/locality/view/' + id);
    };

    return factory;
}
;