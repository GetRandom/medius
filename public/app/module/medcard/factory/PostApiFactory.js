function postApiFactory($http) {
    var factory = {};

    factory.all = function () {
        return $http.get('/api/digest/get-posts');
    };

    factory.add = function (post) {
        return  $http({
            method: 'POST',
            url: '/api/digest/add-post',
            data: $.param(post),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
    };

    factory.update = function (post) {
        return  $http({
            method: 'POST',
            url: '/api/digest/update-post',
            data: $.param(post),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
    };

    factory.remove = function (post) {
        return  $http({
            method: 'POST',
            url: '/api/digest/remove-post',
            data: $.param(post),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
    };

    return factory;
}
;