function ObservationApiFactory($http) {
    var factory = {};

    factory.getPressureById = function (medcardId) {
        return $http.get('/api/pressure/all/' + medcardId);
    };

    factory.getPulseById = function (medcardId) {
        return $http.get('/api/pulse/all/' + medcardId);
    };

    factory.getTemperatureById = function (medcardId) {
        return $http.get('/api/temperature/all/' + medcardId);
    };

    factory.addTemperatureById = function (medcardId, temp) {
        temp.medcard = medcardId;
        return $http.post('/api/temperature/update', temp);
    };
    factory.deleteTemperatureById = function (id) {
        return $http.post('/api/temperature/remove', {id: id});
    };

    factory.addPulseById = function (medcardId, pulse) {
        pulse.medcard = medcardId;
        return $http.post('/api/pulse/update', pulse);
    };

    factory.deletePulseById = function (id) {
        return $http.post('/api/pulse/remove', {id: id});
    };

    factory.addPressureById = function (medcardId, pressure) {
        pressure.medcard = medcardId;
        return $http.post('/api/pressure/update', pressure);
    };

    factory.deletePressure = function (id) {
        return $http.post('/api/pressure/remove', {id: id});
    };

    return factory;
}
;